package ir.novinapproid.test.recordingaudiopart.model;

import android.net.Uri;

import java.io.File;

/**
 * Created by mohammadreza on 7/23/2018.
 */

public class Audio {

    private Uri uri;
    private File file;

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }
}

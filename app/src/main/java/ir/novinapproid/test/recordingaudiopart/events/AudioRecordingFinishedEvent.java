package ir.novinapproid.test.recordingaudiopart.events;

import ir.novinapproid.test.recordingaudiopart.model.Audio;

/**
 * Created by mohammadreza on 7/23/2018.
 */

public class AudioRecordingFinishedEvent {

    private Audio audio;

    public AudioRecordingFinishedEvent(Audio audio) {
        this.audio = audio;
    }

    public Audio getAudio() {
        return audio;
    }
}

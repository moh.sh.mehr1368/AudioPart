package ir.novinapproid.test.recordingaudiopart.managers;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import java.io.File;

import ir.novinapproid.test.recordingaudiopart.model.Audio;

/**
 * Created by mohammadreza on 7/23/2018.
 */

public class FileManager {

    private static final String TAG = FileManager.class.getSimpleName();
    private static FileManager mFileManager;

    public static synchronized FileManager getInstance() {
        if (mFileManager == null) {
            mFileManager = new FileManager();
        }
        return mFileManager;
    }

    public void deleteFile(File file){
        Log.i(TAG,"deleteFile(File file)");

        boolean deleted = file.delete();
        if (deleted){

            Log.i(TAG,"file deleted");
        }
    }

    public void deleteFile(Uri uri){

        Log.i(TAG,"deleteFile(Uri uri)");

        File f = new File(uri.getPath());

        boolean deleted = f.delete();
        if (deleted){

            Log.i(TAG,"file deleted");
        }

    }
}

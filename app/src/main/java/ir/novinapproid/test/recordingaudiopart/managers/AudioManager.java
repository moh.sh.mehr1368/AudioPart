package ir.novinapproid.test.recordingaudiopart.managers;


import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.github.piasy.rxandroidaudio.AudioRecorder;
import com.github.piasy.rxandroidaudio.PlayConfig;
import com.github.piasy.rxandroidaudio.RxAudioPlayer;

import java.io.File;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AudioManager {

    private static final String TAG = AudioManager.class.getSimpleName();
    private AudioRecorder mAudioRecorder;
    private File mAudioFile;
    private RxAudioPlayer mRxAudioPlayer;

    public AudioManager() {
    }

    public void recordAudio(){

        Log.i(TAG,"start recording");
        mAudioRecorder = AudioRecorder.getInstance();
        mAudioFile = new File(
                Environment.getExternalStorageDirectory().getAbsolutePath() +
                        File.separator + System.nanoTime() + ".file.m4a");
        mAudioRecorder.prepareRecord(MediaRecorder.AudioSource.MIC,
                MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.AudioEncoder.AAC,
                mAudioFile);
        mAudioRecorder.startRecord();
    }


    public void stopRecordAudio(){

        Log.i(TAG,"stop recording");
        mAudioRecorder.stopRecord();
    }

    public Uri getAudioFIleUri(){

        return Uri.fromFile(mAudioFile);
    }


    public File getmAudioFile() {
        return mAudioFile;
    }
}

package ir.novinapproid.test.recordingaudiopart.ui.frags;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import ir.novinapproid.test.recordingaudiopart.R;
import ir.novinapproid.test.recordingaudiopart.managers.FileManager;
import nl.changer.audiowife.AudioWife;

import static ir.novinapproid.test.recordingaudiopart.ui.frags.FragConstants.AUDIO_FILE;

/**
 * Created by mohammadreza on 7/23/2018.
 */

public class PlayAudioFrag extends Fragment implements OnClickListener {

    public final static String TAG =PlayAudioFrag.class.getSimpleName();

    private Uri uri;
    private  View rootView;
    private AudioWife audioWife;
    private AppCompatImageView iv_remove;
    private FrameLayout fl_container;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate()");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG,"onCreateView()");

        rootView = inflater.inflate(R.layout.play_audio_frag, container, false);
        iv_remove = rootView.findViewById(R.id.iv_remove);
        fl_container = rootView.findViewById(R.id.fl_container);
        iv_remove.setOnClickListener(this);

        getData();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i(TAG,"onActivityCreated()");

        audioWife = new AudioWife();
        audioWife.init(getContext(), uri)
                .useDefaultUi(fl_container, getLayoutInflater());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,"onPause()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i(TAG,"onDetach()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG,"onDestroyView()");
        audioWife.release();
    }

    @Override
    public void onClick(View v) {
        Log.i(TAG,"iv_remove");

        FileManager.getInstance().deleteFile(uri);
        getActivity().getSupportFragmentManager().popBackStack();

    }

    public void getData() {

        Bundle arguments = getArguments();
        uri = arguments.getParcelable(AUDIO_FILE);
        Log.i(TAG,uri.toString());
    }
}
